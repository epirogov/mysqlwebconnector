﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MySqlWebConnector.Startup))]
namespace MySqlWebConnector
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
